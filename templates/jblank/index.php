<?php
/**
 * J!Blank Template for Joomla by JBlank.pro (JBZoo.com)
 *
 * @package    JBlank
 * @author     SmetDenis <admin@jbzoo.com>
 * @copyright  Copyright (c) JBlank.pro
 * @license    http://www.gnu.org/licenses/gpl.html GNU/GPL
 * @link       http://jblank.pro/ JBlank project page
 */

defined('_JEXEC') or die;


// init $tpl helper
require dirname(__FILE__) . '/php/init.php';

?><?php echo $tpl->renderHTML(); ?>
<head>
    <script src="templates/jblank/js/jquery-1.11.3.min.js"></script>
    <script src="templates/jblank/js/bootstrap.min.js"></script>

    <jdoc:include type="head" />
	<link href="templates/jblank/css/bootstrap.min.css" rel="stylesheet">
	<link href="templates/jblank/css/common.css" rel="stylesheet">
	<link href="templates/jblank/css/custom.css" rel="stylesheet">
</head>
<body class="<?php echo $tpl->getBodyClasses(); ?>">

	<div class="header-main full-width" align="left">

		<div class="column" align="left">
            <img class="logo" src="templates/jblank/images/logo.png" style="margin-left: 100px"/>
			<img src="templates/jblank/images/name.png"/>
		</div>
	</div>
	<div class="after-header full-width"></div>
		<div class="container">

			<div class="row">
				<div class="col-md-2">

                    <jdoc:include type="module" name="mod_bt_login" />
					<ul class="nav nav-pills nav-stacked menu">
						<li ><a href="index.php?option=com_lists">Главная</a></li>
						<li name="viewLists"><a href="index.php?option=com_lists">Просмотр таблиц</a></li>
						<li name="creationTable"><a href="index.php?option=com_lists&view=CreateTable">Создать таблицу</a></li>
					</ul>
				</div>
				<div class="col-md-8">
					<jdoc:include type="component" />
				</div>
				<div class="col-md-2">
					<h1 class="title">Новости<h1>
					<p class="lead article">Новости отсутсвуют</p>
				</div>
			</div>
	</div>
    <!-- partial example -->
    <?php echo $tpl->partial('counters', array(
        'myVar' => 123
    ));?>

    <?php if ($tpl->isDebug()) : ?>
        <jdoc:include type="modules" name="debug" />
    <?php endif; ?>

    <script src="templates/jblank/js/custom.js"></script>
</body></html>