<?php
defined('_JEXEC') or die('Restricted access');

class ListsController extends JControllerLegacy
{
    public function display($cachable = false, $urlparams = false)
    {
        try {
            // Get the document object.
            $document = JFactory::getDocument();
            $user = JFactory::getUser();
            /*echo 'groups'.$user->groups;*/
//            foreach ($user->groups as $group){
//                echo $group;
//            }
            /*if (!in_array('Author',$user->groups)) {
                echo "Недостаточно прав.\n";
            }*/
            // Set the default view name and format from the Request.
            $viewName = $this->input->getCmd('view', 'AvailableTables');
            $layoutName = $this->input->getCmd('layout', 'default');
            $format = $this->input->getCmd('format', 'html');
            $task = $this->input->getCmd('task', 'default');
            $view = $this->getView($viewName, $format);
            $modelName = $viewName;
            $model = $this->getModel($modelName);
            if ($this->input->getCmd('action') == 'creationTable') {
                /*if (!in_array('Publisher',$user->groups)){
                    echo "Недостаточно прав.\n";
                }else {*/
                    $viewName = 'CreateTable';
                    $layoutName = 'resultCreation';
                    try {
                        $resultCreation = $model->createTable($this->input);
                        if ($resultCreation) {
                            $resultCreation = $model->insertRowInAvailableTables($this->input->getCmd('tableName'),$this->input->getCmd('tableDescription'));
                            $resultCreation = $resultCreation . $model->updateColumnsToUpdate($this->input->getCmd('tableName'), $this->input);
                            if ($_FILES['dataFile']['tmp_name']) {
                                $resultCreation = $resultCreation . $model->loadDataFromFile($this->input->getCmd('tableName'));
                            }
                        }
                    } catch (Exception $e) {
                        $resultCreation = $e->getMessage();
                    }
                    if ($resultCreation != null) {
                        if ($resultCreation) {
                            $model->setQuery('Успешное создание таблицы ' . $this->input->getCmd('tableName') . ".\n" . $resultCreation);
                        } else {
                            $model->setQuery('Ошибка создания таблицы ' . $this->input->getCmd('tableName'));
                        }
                    }
            //    }

            }
            if ($viewName == 'ViewTable') {
                $model->setTableName($this->input->getCmd('tableName'));
                $model->getTableRows();
                if ($task == 'update') {
                    $model->updateRows($this->input->getCmd('tableName'), $_REQUEST['toUpdate']);
                }
                if ($task == 'add') {
                    $model->addRows($this->input->getCmd('tableName'), $_REQUEST['toAdd']);
                }
            }


            // Push the model into the view (as default).
            $view->setModel($model, true);
            $view->setLayout($layoutName);

            // Push document object into the view.
            $view->document = $document;

            $view->display();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}