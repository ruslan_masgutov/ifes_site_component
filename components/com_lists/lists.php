<?php
defined('_JEXEC') or die('Restricted access');

//JTable::addIncludePath(JPATH_COMPONENT.'/tables');
require_once JPATH_COMPONENT . '/models/ListsModelAvailableTables.php';
require_once JPATH_COMPONENT . '/models/ListsModelCreateTable.php';
require_once JPATH_COMPONENT . '/models/ListsModelViewTable.php';
//JModelLegacy::addIncludePath(JPATH_COMPONENT . '/models','ListsModel');
//JLoader::registerPrefix('Lists',JPATH_COMPONENT);
$app = JFactory::getApplication();
$input = $app->input;
/*$controller = $input->get('controller','default');*/
$controller = JControllerLegacy::getInstance('Lists');
$controller->execute($input->getCmd('task'));
$controller->redirect();

/*$controller->execute($input->getCmd('task'));*/
/*$classname = 'ListsControllers'.ucwords($controller);
$controller = new $classname();
$controller->execute();
$controller->redirect();*/