/**
 * Created by masrr on 29.05.2015.
 */
var init = function () {
    $('input#update').hide();
    $('#result').hide();
    $('#addNewRecord').hide();
    $('#showFullRecord').hide();
    $('#showInfoData').hide();
//event show full data
    $('#showInfoData').on('click', function () {
        $('#infoData').show();
        $('#add').show();
        $('#update').hide();
        $('#showFullRecord').hide();
        $('#showInfoData').hide();
    });
//show full data record
    $('#infoData tbody tr').on('click', function () {
        $('#infoData').hide();
        $('#add').hide();
        var placesForData = $('#showFullRecord tr td input'),
            data = $(this).find('td span');
        for (var i = 0; i < data.length; i++) {
            $(placesForData[i]).val($(data[i]).html());
        }
        $('#showFullRecord').show();
        $('#showInfoData').show();
        $('#update').show();
    });
//update record
    $('#update').on('click', function () {
        var json = {};
        var row = {};
        $('#showFullRecord tr').each(function (index, elem) {
            elem = $(elem).find('td');
            row[$(elem[0]).html()] = $(elem[1]).find('input').prop('value');
        });
        json['tr0'] = row;
        var data = JSON.stringify(json);
        $.ajax(document.location.href + '&format=raw&task=update', {
            method: "POST",
            data: {'toUpdate': data}})
            .done(function (response) {
                $('#result').show().html('').append(response);
                document.location.href = document.location.href;
            });
    });
//Add new record
    $('input#add').on('click', function () {
        if ($(this).hasClass('btn-primary')) {
            $('#addNewRecord').show();
            $(this).removeClass('btn-primary').addClass('btn-danger');
        } else {
            var json = {}
            var row = {};
            $('#addNewRecord tbody tr').each(function (index, elem) {
                elem = $(elem).find('td');
                row[$(elem[0]).html()] = $(elem[1]).find('input').prop('value');
            });
            json['tr0'] = row;
            var data = JSON.stringify(json);
            $.ajax(document.location.href + '&format=raw&task=add', {
                method: "POST",
                data: {'toAdd': data}})
                .done(function (response) {
                    $('input#add').removeClass('btn-danger').addClass('btn-primary');
                    $('#addNewRecord').hide();
                    $('#result').show().html('').append(response);
                    document.location.href = document.location.href;
                });
        }
    });
};