/**
 * Created by masrr on 29.05.2015.
 */
var init = function () {
    function getColumnNumber() {
        if ($('form#createTable div.column').length > 0) {
            return $('form#createTable div.column:last').data('num') + 1;
        } else {
            return 1;
        }
    }

    $('#addColumn').on('click', function () {
        var columnNum = getColumnNumber();
        var templ = '<div class="form-group column" data-num="' + columnNum + '">' +
            '   <label for="tableName">Столбец</label>' +
            '   <input type="text" class="form-control" name="columnName' + columnNum + '" id="columnName" placeholder="Название столбца" maxlength="25"/>' +
            '   <select class="form-control" name="dataType' + columnNum + '" id="dataType">' +
            '       <option value="">Выберите тип хранимых данных</option>' +
            '       <option value="VARCHAR">Текст</option>' +
            '       <option value="INT">Число</option>' +
            '   </select>' +
            '   <input style="display: none;" type="text" class="form-control" name="precisionDataType' + columnNum + '" id="precisionDataType" placeholder="Точность данных" maxlength="10"/>' +
            '   <div class="checkbox"><label><input name="allowNull' + columnNum + '" checked type="checkbox">Возможны пустые значения</label></div>' +
            '   <div class="checkbox"><label><input name="toPreShow' + columnNum + '" checked type="checkbox">Для предварительного показа</label></div>' +
            '</div>';
        $('input#addColumn').before(templ);
        $('div.form-group[data-num="' + columnNum + '"] select').on('change', function () {
            if ($(this).val() == 'VARCHAR') {
                $('div.form-group[data-num="' + columnNum + '"] input#precisionDataType').show();
            } else {
                $('div.form-group[data-num="' + columnNum + '"] input#precisionDataType').hide();
            }
        })
    })
};