<?php
defined('_JEXEC') or die('Restricted access');

class ListsModelCreateTable extends JModelItem{
    private $query;

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }
    public function createTable($input = null)
    {
        $db = JFactory::getDbo();
        $columnList = '';
        $counter = 1;
        while (true) {
            if ($input->getCmd('columnName' . strval($counter)) != null) {
                $columnList = $columnList . ' ' . $db->quoteName($input->getCmd('columnName' . strval($counter))) . ' ' .
                    $input->getCmd('dataType' . strval($counter)) . '(' .
                    $input->getCmd('precisionDataType' . strval($counter)) . ')';
                if ($input->getCmd('allowNull' . strval($counter)) == 'on') {
                    $columnList = $columnList . ', ';
                } else {
                    $columnList = $columnList . ' NOT NULL, ';
                }
                $counter += 1;
            } else {
                break;
            }
        }
        //TODO replace scheme name
        $query = 'CREATE TABLE `site`.`#__lists_' . $input->getCmd('tableName') . '`(
                `id` INT(11) NOT NULL AUTO_INCREMENT,' . $columnList .
            'PRIMARY KEY (`id`)
            )
            ENGINE =MyISAM
            AUTO_INCREMENT =0
            DEFAULT CHARSET =utf8;';
        $db->setQuery($query);
        return $db->execute();

    }
    public function updateColumnsToUpdate($tableName, $input){
        $db = JFactory::getDbo();
        $counter = 1;
        $toPreShow ='';
        while (true) {
            if ($input->getCmd('columnName' . strval($counter)) != null) {
                if ($input->getCmd('toPreShow' . strval($counter)) == 'on'){
                    if ($toPreShow==''){
                        $toPreShow = $input->getCmd('columnName' . strval($counter));
                    }else{
                        $toPreShow = $toPreShow . ',' . $input->getCmd('columnName' . strval($counter));
                    }

                }
                $counter += 1;
            } else {
                break;
            }
        }
        if ($toPreShow!='') {
            $query = 'UPDATE `#__lists_availabletables` SET `columnsToPreShow`=\'' . $toPreShow . '\' WHERE `tableName`=\'' . $tableName . '\'';
            $db->setQuery($query);
            if ($db->execute()) {
                return "Заданы столбцы для предварительного отображения.\n";
            }else{
                return "Ошибка задания столбцов для предварительного отображения.\n";
            }
        }
        return '';
    }
    public function insertRowInAvailableTables($name, $description){
        $db = JFactory::getDbo();
        $query = 'INSERT INTO `#__lists_availabletables` (`tableName`,`description`,`name`,`lastModifyTime`) VALUES (\''
            . $name . '\',\'' . $description . '\',\'' . $name . '\',\'' . date(DATE_ATOM) . '\');';
        $db->setQuery($query);
        if ($db->execute()) {
            return "Таблица добавлена в список доступных таблиц.\n";
        }else{
            return "Ошибка добавления таблицы в список доступных таблиц.\n";
        }
    }
    public function loadDataFromFile($tableName){
        $uploadfile = JPATH_COMPONENT_SITE . '/uploadedFiles/' . basename($_FILES['dataFile']['name']);
        if (move_uploaded_file($_FILES['dataFile']['tmp_name'], $uploadfile)) {
            if (($handle = fopen($uploadfile, "r"))!==FALSE){
                $row = 0;
                $columnNames ='';
                $tableData='';
                while(($data = fgetcsv($handle))!== FALSE){
                    if($row==0){
                        for($i = 0; $i < count($data); $i++){
                            if ($columnNames==''){
                                $columnNames = '`'.$data[$i].'`';
                            }else{
                                $columnNames = $columnNames.',`'.$data[$i].'`';
                            }
                        }
                        $columnNames = '('.$columnNames.')';
                    }else{
                        $rowData = '';
                        for($i = 0; $i < count($data); $i++){
                            if ($rowData==''){
                                $rowData = '\''.$data[$i].'\'';
                            }else{
                                $rowData = $rowData.',\''.$data[$i].'\'';
                            }
                        }
                        if ($tableData==''){
                            $tableData = '('.$rowData.')';
                        }else{
                            $tableData = $tableData.',('.$rowData.')';
                        }

                    }
                    $row++;
                }
                $db = JFactory::getDbo();
                $query = 'INSERT INTO `#__lists_'.$tableName.'` '.$columnNames.' VALUES '.$tableData.';';
                $db->setQuery($query);
                $resultQuery = $db->execute();
                if ($resultQuery){
                    return "Данные успешно загружены.\n";
                }else{
                    return "Ошибка загрузки данных.\n".$resultQuery;
                }
                fclose($handle);
            }
            return "Файл корректен и был успешно загружен.\n";
        } else {
            return "Возможная атака с помощью файловой загрузки!\n";
        }

    }
}