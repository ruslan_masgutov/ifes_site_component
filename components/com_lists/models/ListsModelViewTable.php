<?php
defined('_JEXEC') or die('Restricted access');
class ListsModelViewTable extends JModelItem{
    private $rows;
    private $tableName;
    private $resultOperation;

    /**
     * @return mixed
     */
    public function getResultOperation()
    {
        return $this->resultOperation;
    }

    /**
     * @param mixed $resultUpdate
     */
    public function setResultOperation($resultUpdate)
    {
        $this->resultOperation = $resultUpdate;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param mixed $tableName
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }
    /**
     * @return mixed
     */
    public function getRows()
    {
        return $this->rows;
    }
    public function getAllTableColumns(){
        $db = JFactory::getDbo();
        $query='SHOW COLUMNS FROM `#__lists_'.$this->getTableName().'`;';
        //$query = "SELECT columnsToPreShow FROM `site`.`site_lists_availabletables` where tableName='".$this->getTableName()."';";
        $db->setQuery((string)$query);
        return $db->loadRowList();
    }
    public function getTableColumns(){
        $db = JFactory::getDbo();
        //$query='SHOW COLUMNS FROM `site`.`site_lists_'.$this->getTableName().'`;';
        $query = "SELECT columnsToPreShow FROM `#__lists_availabletables` where tableName='".$this->getTableName()."';";
        $db->setQuery((string)$query);
        return $db->loadRowList();
    }
    public function getTableRows(){
        if(!is_array($this->rows)){
            $this->rows = array();
        }
        $db = JFactory::getDbo();
        //$columnsToPreShow = $this->getTableColumns();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__lists_'.$this->getTableName());
        $db->setQuery((string)$query);
        $this->rows = $db->loadRowList();
        return $this->rows;
    }
    public function addRows($tableName, $rows){
        try{
            $this->setResultOperation('');
            $rows = json_decode(urldecode($rows),true);
            $db = JFactory::getDbo();
            $columns = '';
            foreach ($rows['tr0'] as $key=>$el) {
                if ($columns == '') {
                    $columns = '`'.$key.'`';
                } else {
                    $columns = $columns . ',`' .$key.'`';
                }
            }
            $resultData = '';
            foreach ($rows as $row) {
                $data = '';
                foreach ($row as $el) {
                    if ($data == '') {
                        $data = '\'' . $el . '\'';
                    } else {
                        $data = $data . ',' . '\'' . $el . '\'';
                    }
                }
                if ($resultData == '') {
                    $resultData = '(' . $data . ')';
                } else {
                    $resultData = $resultData . ',' . '(' . $data . ')';
                }
            }
            $query = 'INSERT INTO `#__lists_' . $tableName . '`(' . $columns . ')'
                . ' VALUES  ' . $resultData . ';';
            $db->setQuery($query);
            $this->resultOperation +=$db->execute();
        }catch(Exception $e){
            $this->resultOperation .= $e->getMessage();
        }
    }
    public function updateRows($tableName, $rows){
        try {
            $this->setResultOperation('');
            $rows = json_decode(urldecode($rows),true);
            $db = JFactory::getDbo();
            foreach($rows as $row){
                $injectStr='';
                $where='';
                foreach($row as $key=>$value){
                    if($key=='id'){
                        $where = 'id ='.$value;
                    }else {
                        if ($injectStr == '') {
                            $injectStr = $key . '=' . $db->quote($value);
                        } else {
                            $injectStr .= ',' . $key . '=' . $db->quote($value);
                        }
                    }
                }
                $query = 'UPDATE `#__lists_'.$tableName.'` SET '.$injectStr.' WHERE '.$where.';';
                $db->setQuery($query);
                $this->resultOperation +=$db->execute();
            }
        }catch (Exception $e){
            $this->resultOperation .= $e->getMessage();
        }
    }
}