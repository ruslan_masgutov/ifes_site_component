<?php
defined('_JEXEC') or die('Restricted access');
class ListsModelAvailableTables extends JModelItem{
    protected $tables;

    public function getAvailableTables(){
        if(!is_array($this->tables)){
            $this->tables = array();
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('tableName,name,description,lastModifyTime');
        $query->from('#__lists_availabletables');
        $db->setQuery((string)$query);
        $this->tables = $db->loadRowList();
        return $this->tables;
    }
}