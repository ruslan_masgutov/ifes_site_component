<?php
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript('/site/components/com_lists/assets/js/ViewTable.js')
?>
<table class="table" id="addNewRecord">
    <caption>Новая запись</caption>
    <thead><tr><th>Поле</th><th>Данные</th></tr></thead>
    <tbody>
    <?php
    foreach($this->columns as $row){
        if($row[0]!='id') {
            echo '<tr><td>' . $row[0] . '</td><td><div class="form-group"><input type="text" class="form-control" value=""></div></td></tr>';
        }
    }
    ?>
    </tbody>
</table>
<table class="table" id="showFullRecord">
    <caption>Подробный просмотр записи</caption>
    <thead><tr><th>Поле</th><th>Данные</th></tr></thead>
    <tbody>
    <?php
    foreach($this->columns as $row){
        if($row[0]!='id') {
            echo '<tr><td>' . $row[0] . '</td><td><div class="form-group"><input type="text" class="form-control" value=""></div></td></tr>';
        }else{
            echo '<tr style="display:none;"><td>' . $row[0] . '</td><td><div class="form-group"><input type="text" class="form-control" value=""></div></td></tr>';
        }
    }
    ?>
    </tbody>
</table>

<input id="add" class="btn btn-primary" type="button" value="Добавить запись"/>
<input id="search" class="btn btn-primary" type="button" value="Поиск"/>
<table class="table" id="infoData">
    <caption>Данные таблицы <?php echo $this->tableName;?></caption>
    <thead>
        <tr>
            <?php
            $count = 0;
            $columnsToShow = ';';
            foreach($this->columns as $column){
                if(strpos($this->columnsToShow,$column[0])===FALSE) {
                    echo '<th style="display:none;">' . $column[0] . '</th>';
                }else{
                    echo '<th>' . $column[0] . '</th>';
                    $columnsToShow = $columnsToShow.$count.';';
                }
                $count++;
            }?>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($this->rows as $row){
                echo '<tr>';
                foreach($row as $key=>$column) {
                    if($key=='id'){
                        echo '<td class="id" style="display:none;"><span>' . $column . '</span></td>';
                    }else {
                        if(strpos($columnsToShow,';'.$key.';')===FALSE) {
                            echo '<td style="display:none;"><span>' . $column . '</span></td>';
                        }else{
                            echo '<td><span>' . $column . '</span></td>';
                        }
                    }
                }
                echo '</tr>';
            }
        ?>
</tbody>
</table>
<input id="showInfoData" class="btn btn-primary" type="button" value="Вернуться"/>
<input id="update" class="btn btn-primary" type="button" value="Обновить"/>
<div id="result" class="bs-callout bs-callout-info"></div>
<script>
    init();
</script>