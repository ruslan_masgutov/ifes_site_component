<?php
defined('_JEXEC') or die('Restricted access');
class ListsViewViewTable extends JViewLegacy{
    function display($tpl=null){
        $input = JFactory::getApplication()->input;
        $this->columnsToShow = $this->get('TableColumns');
        $this->columnsToShow = $this->columnsToShow[0][0];
        $this->rows= $this->get('TableRows');
        $this->tableName = $this->get('TableName');
        $this->columns = $this->get('AllTableColumns');
        parent::display($tpl);
    }
}