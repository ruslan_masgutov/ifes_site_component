<?php
defined('_JEXEC') or die('Restricted access');
?>
    <form id="createTable" enctype="multipart/form-data" method="post">
        <input type="text" style="display: none" name="action" value="creationTable"/>

        <div class="form-group">
            <label for="tableName">Название таблицы</label>
            <input type="text" class="form-control" name="tableName" id="tableName" placeholder="Название таблицы"
                   maxlength="25"/>
        </div>
        <div class="form-group" style="margin: 10px 0px 10px 0px">
            <label for="tableDescription">Описание таблицы</label>
            <textarea class="form-control" rows="3" name="tableDescription" id="tableDescription"
                      placeholder="Описание таблицы"></textarea>
        </div>
        <input id="addColumn" type="button" class="btn btn-primary" value="Добавить столбец"
               style="margin: 10px 0px 10px 0px"/>

        <div class="form-group">
            <label for="dataFile">Данные</label>
            <input type="file" name="dataFile"/>

            <p class="help-block">Данные для таблицы в формате CSV</p>
        </div>
        <button type="submit" class="btn btn-primary">Создать</button>
    </form>
<?php
JFactory::getDocument()->addScript('/site/components/com_lists/assets/js/CreateTable.js');
?>
<script>
    init();
</script>