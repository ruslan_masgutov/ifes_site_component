<?php
defined('_JEXEC') or die('Restricted access');

class ListsViewAvailableTables extends JViewLegacy{
    function display($tpl=null){
        $this->AvailableTables = $this->get('AvailableTables');
        parent::display($tpl);
    }
}