<?php
defined('_JEXEC') or die('Restricted access');
JFactory::getDocument()->addScript('/site/components/com_lists/assets/js/AvailableTables.js')
?>
<table class="table">
    <caption>Доступные таблицы</caption>
    <thead>
        <tr>
            <th style="width: 200px">Название таблицы</th>
            <th>Описание</th>
            <th style="width: 250px">Дата последней модификации</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($this->AvailableTables as $table){
                echo '<tr onclick="viewTable(\''.$table[0].'\')"><td>'.$table[0].'</td><td>'.$table[2].'</td><td>'.$table[3].'</td></tr>';
            }
        ?>
    </tbody>
</table>
<script>
    init();
</script>

